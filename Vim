"Vundle for new vim install
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
:PluginInstall!
:OmniSharpStartServer ~/RiderProjects/NewProject

:ddp-J                  "flip two lines and merge (delete, paste, join above)

:gg=G\cf                "gg top of file, = code format, G end of file, \cf Omnisharp Code Format

:wa                     ":wall, :wqa or :xa to exit and save all buffers

:setf cs                "set format to csharp

:args `find . -type f`  "Set files for vimgrep
:vim /pattern1/ ##

ctrl+ww                  "switch window

+y                       "register, yank/paste/cut.. ie < c-"+p >
+p
+x

\cf                      "OmniSharp Code Format

ctrl+p ctrl+f/b          "CtrlP toggle modes, ? in CtrlP for mappings

:%s/Search/Replace       "Search/Replace entire file

:%s/^M//g                "Remove  Characters (Ctrl V + Ctrl M)

set -n l                 "Verify characters the keyboard is sending to the terminal

execute "set <M-j>=\ej"  "Map Alt or Esc \e is ^[ sent by the Alt key
nnoremap <M-j> j

u                        "Undo in Linux Vim
Ctrl + r                 "Redo in Linux Vim

Ctrl + f / b             "Page Up / Page Down

ci"                      "Change In "  " .... deletes text between two quotes

cw                       "Change Word
caw
ciw

c$                       "Change to end of line 
C

cc                       "Change entire line
cis                      "Change to end of sentence

h,j,k,l                  "Movement
w,e,b                    "w - start next word, e - end, b - beginning
f, F                     "find next occurence of character
%                        " Navigate to matching ( { [
0, $                     "Beginning / End of line
*, #                     "Next/Prev word occurrence
g, G, G19                "gg begging of file, G is end of file, G19 got to line 19
/, n, N                  "/ search, n next, N prev occurrence
o, x, r, d, v, .         "new line, del letter, replace letter, delete, visual, . repeat

set visualbell           "turn off bell
